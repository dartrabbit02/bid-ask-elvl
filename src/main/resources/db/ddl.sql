drop table if exists E_LEVEL;
drop table if exists QUOTE;

create table E_LEVEL
(
  ISIN          varchar(12) primary key,
  E_LEVEL_VALUE decimal(20, 2) not null
);

create table QUOTE
(
  INSSTMP timestamp default now(),
  ISIN     varchar(12),
  BID      decimal(20, 2),
  ASK      decimal(20, 2),
  BID_SIZE decimal(20, 2),
  ASK_SIZE decimal(20, 2)
);

--create index if not exists QUOTE_BID_IDX on QUOTE (BID);
