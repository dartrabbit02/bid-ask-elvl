package com.bidask.elvl.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class JSONWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(JSONWriter.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    public String writeJSON(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error while writing JSON response", e);
        }
        return "Error";
    }
}
