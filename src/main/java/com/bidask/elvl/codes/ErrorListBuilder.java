package com.bidask.elvl.codes;

import com.bidask.elvl.dto.response.CommonResponse;
import com.bidask.elvl.dto.response.HeaderError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ErrorListBuilder {

    private ResponseCode responseStatus = ResponseCode.OK;
    private final List<HeaderError> headerErrors = Collections.synchronizedList(new ArrayList<>());

    /**
     * Checks failure condition reached.
     * @return {@code true} if response status is not OK.
     */
    public boolean isFailure() {
        return responseStatus.getCode() > ResponseCode.OK.getCode();
    }

    public void error(Throwable throwable) {
        responseStatus = ResponseCode.INTERNAL_SERVER_ERROR;
        headerErrors.add(new HeaderError(ErrorCode.UNHANDLED_ERROR.getCode(), throwable.getMessage()));
    }

    public void error(ErrorCode errorCode){
        responseStatus  = errorCode.getResponseCode();
        headerErrors.add(new HeaderError(errorCode.getCode(), errorCode.getMessage()));
    }

    public CommonResponse buildResponse() {
        CommonResponse response = new CommonResponse();
        response.setCode(responseStatus.getCode());
        response.setResponse(responseStatus.getMessage());
        response.setErrors(headerErrors);
        return response;
    }
}