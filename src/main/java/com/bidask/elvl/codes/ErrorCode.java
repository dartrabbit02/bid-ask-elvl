package com.bidask.elvl.codes;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCode {

    UNHANDLED_ERROR(ResponseCode.INTERNAL_SERVER_ERROR,  1000, ""),
    ISIN_IS_NOT_FOUND(ResponseCode.NOT_FOUND, 2000, "Current ISIN not found"),
    ISIN_IS_EMPTY_OR_HAS_WRONG_LENGTH(ResponseCode.BAD_REQUEST, 2001, "ISIN is empty or has wrong length"),
    QUOTES_BID_AND_ASK_HAS_WRONG_VALUES(ResponseCode.BAD_REQUEST, 3000, "Quote's 'bid' and 'ask' values are wrong");

    private final ResponseCode responseCode;
    private final Integer code;
    private final String message;

}
