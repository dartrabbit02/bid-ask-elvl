package com.bidask.elvl.codes;

import lombok.Getter;

@Getter
public enum ResponseCode {
    OK(200, "OK"),
    BAD_REQUEST(400, "BAD_REQUEST"),
    NOT_FOUND(404, "NOT_FOUND"),
    INTERNAL_SERVER_ERROR(500, "INTERNAL_SERVER_ERROR")
    ;

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * Numerical response code.
     */
    private final int code;

    /**
     * Response message.
     */
    private final String message;
}

