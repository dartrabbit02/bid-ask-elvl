package com.bidask.elvl.service;

import com.bidask.elvl.codes.ErrorCode;
import com.bidask.elvl.codes.ErrorListBuilder;
import com.bidask.elvl.dto.quote.QuoteResponse;
import com.bidask.elvl.dto.quote.QuoteTO;
import com.bidask.elvl.entity.Quote;
import com.bidask.elvl.mapper.EntitiesAndTOMapper;
import com.bidask.elvl.repository.EnergyLevelRepository;
import com.bidask.elvl.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuoteService {

    private final QuoteRepository quoteRepository;
    private final EnergyLevelRepository energyLevelRepository;

    @Autowired
    public QuoteService(QuoteRepository quoteRepository,
                        EnergyLevelRepository energyLevelRepository) {
        this.quoteRepository = quoteRepository;
        this.energyLevelRepository = energyLevelRepository;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public QuoteResponse save(QuoteTO quoteTO, ErrorListBuilder errorListBuilder) {
        Quote quote = EntitiesAndTOMapper.mapToEntity(quoteTO);
        if (quoteRepository.saveQuote(quote)) {
            energyLevelRepository.saveEnergyLevel(quote);
            return QuoteResponse.builder()
                    .quoteTO(EntitiesAndTOMapper.mapToTo(quote))
                    .build();
        }
        errorListBuilder.error(ErrorCode.UNHANDLED_ERROR);
        return null;
    }
}