package com.bidask.elvl.service;

import com.bidask.elvl.dto.elevl.EnergyLevelResponse;
import com.bidask.elvl.entity.EnergyLevel;
import com.bidask.elvl.mapper.EntitiesAndTOMapper;
import com.bidask.elvl.repository.EnergyLevelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnergyLevelService {

    private final EnergyLevelRepository energyLevelRepository;

    @Autowired
    public EnergyLevelService(EnergyLevelRepository energyLevelRepository) {
        this.energyLevelRepository = energyLevelRepository;
    }

    public EnergyLevelResponse getEnergyLevelByISIN(String isin) {
        EnergyLevel energyLevel = energyLevelRepository.getEnergyLevelByISIN(isin);
        EnergyLevelResponse response = new EnergyLevelResponse();
        if (energyLevel != null) {
            response.getEnergyLevelTOs().add(EntitiesAndTOMapper.mapToTO(energyLevel));
        }
        return response;
    }

    public EnergyLevelResponse getAllEnergyLevels() {
        List<EnergyLevel> allEnergyLevels = energyLevelRepository.getAllEnergyLevels();
        EnergyLevelResponse response = new EnergyLevelResponse();
        allEnergyLevels.forEach(energyLevel ->
            response.getEnergyLevelTOs().add(EntitiesAndTOMapper.mapToTO(energyLevel))
        );
        return response;
    }
}
