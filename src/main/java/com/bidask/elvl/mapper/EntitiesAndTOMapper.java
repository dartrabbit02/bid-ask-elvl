package com.bidask.elvl.mapper;

import com.bidask.elvl.dto.elevl.EnergyLevelTO;
import com.bidask.elvl.dto.quote.QuoteTO;
import com.bidask.elvl.entity.EnergyLevel;
import com.bidask.elvl.entity.Quote;

import java.util.Date;

public class EntitiesAndTOMapper {

    private EntitiesAndTOMapper() {
    }

    public static Quote mapToEntity(QuoteTO quoteTO) {
        return Quote.builder()
                .insertStamp(new Date())
                .isin(quoteTO.getIsin())
                .bid(quoteTO.getBid())
                .ask(quoteTO.getAsk())
                .bidSize(quoteTO.getBidSize())
                .askSize(quoteTO.getAskSize())
                .build();
    }

    public static QuoteTO mapToTo(Quote quote) {
        return QuoteTO.builder()
                .insertStamp(quote.getInsertStamp())
                .isin(quote.getIsin())
                .bid(quote.getBid())
                .ask(quote.getAsk())
                .bidSize(quote.getBidSize())
                .askSize(quote.getAskSize())
                .build();
    }

    public static EnergyLevelTO mapToTO(EnergyLevel energyLevel){
        return EnergyLevelTO.builder()
                .isin(energyLevel.getIsin())
                .value(energyLevel.getValue())
                .build();
    }
}
