package com.bidask.elvl.dto.response;

import com.bidask.elvl.dto.BasicJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HeaderError implements BasicJson {
    @JsonProperty("code")
    private Integer code;
    @JsonProperty("message")
    private String message;
}