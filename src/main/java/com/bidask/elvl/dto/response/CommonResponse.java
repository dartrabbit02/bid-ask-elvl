package com.bidask.elvl.dto.response;

import com.bidask.elvl.dto.BasicJson;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Data
public class CommonResponse implements BasicJson {
    @JsonProperty("response")
    String response;
    @JsonProperty("status")
    Integer code;
    @JsonProperty("message")
    String message;
    @JsonProperty("errors")
    List<HeaderError> errors;
}
