package com.bidask.elvl.dto.elevl;

import com.bidask.elvl.dto.response.CommonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class EnergyLevelResponse extends CommonResponse {
    @JsonProperty("energyLevels")
    private List<EnergyLevelTO> energyLevelTOs = new ArrayList<>();
}
