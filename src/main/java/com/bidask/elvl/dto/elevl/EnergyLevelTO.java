package com.bidask.elvl.dto.elevl;

import com.bidask.elvl.dto.BaseTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class EnergyLevelTO extends BaseTO {

    @JsonProperty("value")
    private BigDecimal value;

    @Builder
    public EnergyLevelTO(String isin, BigDecimal value) {
        super(isin);
        this.value = value;
    }
}
