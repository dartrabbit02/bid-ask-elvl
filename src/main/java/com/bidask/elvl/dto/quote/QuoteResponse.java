package com.bidask.elvl.dto.quote;

import com.bidask.elvl.dto.response.CommonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class QuoteResponse extends CommonResponse {
    @JsonProperty("quote")
    private QuoteTO quoteTO;
}
