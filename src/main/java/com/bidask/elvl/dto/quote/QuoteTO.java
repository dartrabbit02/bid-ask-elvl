package com.bidask.elvl.dto.quote;

import com.bidask.elvl.dto.BaseTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class QuoteTO extends BaseTO {
    @JsonProperty("insertStamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss.SSS'Z'")
    private Date insertStamp;
    @JsonProperty("bid")
    private BigDecimal bid;
    @JsonProperty("ask")
    private BigDecimal ask;
    @JsonProperty("bidSize")
    private BigDecimal bidSize;
    @JsonProperty("askSize")
    private BigDecimal askSize;

    @Builder
    public QuoteTO(String isin, BigDecimal bid, BigDecimal ask, BigDecimal bidSize, BigDecimal askSize, Date insertStamp) {
        super(isin);
        this.bid = bid;
        this.ask = ask;
        this.bidSize = bidSize;
        this.askSize = askSize;
        this.insertStamp = insertStamp;
    }
}