package com.bidask.elvl.repository;

import com.bidask.elvl.entity.EnergyLevel;
import com.bidask.elvl.entity.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@PropertySource("classpath:query/query.properties")
public class EnergyLevelRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnergyLevelRepository.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Value("${elvl.update.query}")
    private String updateEnergyLevelQuery;
    @Value("${elvl.insert.query}")
    private String insertEnergyLevelQuery;
    @Value("${elvl.getone.query}")
    private String getOneEnergyLevelByISINQuery;
    @Value("${elvl.getall.query}")
    private String getAllEnergyLevelQuery;

    public EnergyLevelRepository(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @CacheEvict("elevel")
    public void saveEnergyLevel(Quote quote) {
        EnergyLevel energyLevel = getEnergyLevelByISIN(quote.getIsin());

        if (energyLevel == null) {
            if (quote.getBid() != null) {
                energyLevel = EnergyLevel.builder()
                        .isin(quote.getIsin())
                        .value(quote.getBid())
                        .build();
            } else {
                energyLevel = EnergyLevel.builder()
                        .isin(quote.getIsin())
                        .value(quote.getAsk())
                        .build();
            }
            insertEnergyLevel(energyLevel);
            LOGGER.info("Added energy level {} for ISIN={}", energyLevel.getValue(), energyLevel.getIsin());
        } else {

            BigDecimal currentEnergyLevelValue = energyLevel.getValue();

            if (quote.getBid() != null && quote.getBid().compareTo(currentEnergyLevelValue) > 0) {
                energyLevel.setValue(quote.getBid());
                updateEnergyLevel(energyLevel);
                LOGGER.info("Updated energy level {} to {} for ISIN={}",
                        currentEnergyLevelValue, energyLevel.getValue(), energyLevel.getIsin());
            } else if (quote.getAsk().compareTo(currentEnergyLevelValue) < 0) {
                energyLevel.setValue(quote.getAsk());
                updateEnergyLevel(energyLevel);
                LOGGER.info("Updated energy level {} to {} for ISIN={}",
                        currentEnergyLevelValue, energyLevel.getValue(), energyLevel.getIsin());
            }
        }
    }

    @Cacheable("elevel")
    public EnergyLevel getEnergyLevelByISIN(String isin) {
        try {
            return jdbcTemplate.queryForObject(getOneEnergyLevelByISINQuery, getIsinSqlParameter(isin), getRowMapper());
        } catch (EmptyResultDataAccessException e) {
            LOGGER.warn("Energy level with ISIN={} not found", isin);
            return null;
        }
    }

    public List<EnergyLevel> getAllEnergyLevels() {
        try {
            return jdbcTemplate.query(getAllEnergyLevelQuery, getRowMapper());
        } catch (EmptyResultDataAccessException e) {
            LOGGER.warn("Energy levels with not found");
            return new ArrayList<>();
        }
    }

    private void insertEnergyLevel(EnergyLevel energyLevel) {
        jdbcTemplate.update(insertEnergyLevelQuery, convertEnergyLevelToSQLParameters(energyLevel));
    }

    private void updateEnergyLevel(EnergyLevel energyLevel) {
        jdbcTemplate.update(updateEnergyLevelQuery, convertEnergyLevelToSQLParameters(energyLevel));
    }

    private RowMapper<EnergyLevel> getRowMapper() {
        return (rs, rowNum) -> EnergyLevel.builder()
                .isin(rs.getString("ISIN"))
                .value(rs.getBigDecimal("E_LEVEL_VALUE"))
                .build();
    }

    private SqlParameterSource getIsinSqlParameter(String isin) {
        return new MapSqlParameterSource()
                .addValue("ISIN", isin);
    }

    private SqlParameterSource convertEnergyLevelToSQLParameters(EnergyLevel energyLevel) {
        return new MapSqlParameterSource()
                .addValue("ISIN", energyLevel.getIsin())
                .addValue("E_LEVEL_VALUE", energyLevel.getValue());
    }
}