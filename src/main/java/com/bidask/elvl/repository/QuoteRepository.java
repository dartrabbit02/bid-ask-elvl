package com.bidask.elvl.repository;

import com.bidask.elvl.entity.Quote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

@Component
@PropertySource("classpath:query/query.properties")
public class QuoteRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuoteRepository.class);

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Value("${quote.insert.query}")
    private String quoteInsertQuery;

    public QuoteRepository(DataSource dataSource) {
        jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public boolean saveQuote(Quote quote) {
        LOGGER.info("Saved QUOTE at {}", quote.getInsertStamp());
        return jdbcTemplate.update(quoteInsertQuery, convertQuoteToSQLParameters(quote)) > 0;
    }

    private SqlParameterSource convertQuoteToSQLParameters(Quote quote) {
        return new MapSqlParameterSource()
                .addValue("INSSTMP", quote.getInsertStamp())
                .addValue("ISIN", quote.getIsin())
                .addValue("BID", quote.getBid())
                .addValue("ASK", quote.getAsk())
                .addValue("BID_SIZE", quote.getBidSize())
                .addValue("ASK_SIZE", quote.getAskSize());
    }
}
