package com.bidask.elvl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElvlApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElvlApplication.class, args);
    }

}