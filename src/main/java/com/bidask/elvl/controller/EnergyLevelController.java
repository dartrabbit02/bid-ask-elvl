package com.bidask.elvl.controller;

import com.bidask.elvl.codes.ErrorCode;
import com.bidask.elvl.codes.ErrorListBuilder;
import com.bidask.elvl.dto.elevl.EnergyLevelResponse;
import com.bidask.elvl.service.EnergyLevelService;
import com.bidask.elvl.utils.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/energy-level/")
public class EnergyLevelController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnergyLevelController.class);
    private final JSONWriter jsonWriter;
    private final EnergyLevelService energyLevelService;

    @Autowired
    public EnergyLevelController(JSONWriter jsonWriter, EnergyLevelService energyLevelService) {
        this.jsonWriter = jsonWriter;
        this.energyLevelService = energyLevelService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public String getEnergyLevelByISIN(@RequestParam(value = "isin") String isin) {
        LOGGER.info("[GET] /energy-level/ for ISIN={}", isin);
        ErrorListBuilder errorListBuilder = new ErrorListBuilder();
        if (StringUtils.isEmpty(isin) || isin.length() != 12) {
            errorListBuilder.error(ErrorCode.ISIN_IS_EMPTY_OR_HAS_WRONG_LENGTH);
        }
        EnergyLevelResponse response = energyLevelService.getEnergyLevelByISIN(isin);
        if (CollectionUtils.isEmpty(response.getEnergyLevelTOs())) {
            errorListBuilder.error(ErrorCode.ISIN_IS_NOT_FOUND);
            return jsonWriter.writeJSON(errorListBuilder.buildResponse());
        }
        return jsonWriter.writeJSON(response);
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllEnergyLevels() {
        LOGGER.info("[GET] /energy-level/all");
        EnergyLevelResponse response = energyLevelService.getAllEnergyLevels();
        return jsonWriter.writeJSON(response);
    }
}
