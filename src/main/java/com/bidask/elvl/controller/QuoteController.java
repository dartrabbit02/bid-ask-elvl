package com.bidask.elvl.controller;

import com.bidask.elvl.codes.ErrorCode;
import com.bidask.elvl.codes.ErrorListBuilder;
import com.bidask.elvl.dto.quote.QuoteResponse;
import com.bidask.elvl.dto.quote.QuoteTO;
import com.bidask.elvl.service.QuoteService;
import com.bidask.elvl.utils.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/quotes/")
public class QuoteController {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuoteController.class);
    private final JSONWriter jsonWriter;
    private final QuoteService quoteService;

    @Autowired
    public QuoteController(JSONWriter jsonWriter,
                           QuoteService quoteService) {
        this.jsonWriter = jsonWriter;
        this.quoteService = quoteService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveQuote(@RequestBody QuoteTO quoteTO) {
        LOGGER.info("[POST] /quotes/ new quote for {}, {}:{} - {}:{}",
                quoteTO.getIsin(), quoteTO.getBid(), quoteTO.getAsk(), quoteTO.getBidSize(), quoteTO.getAskSize());
        ErrorListBuilder errorListBuilder = new ErrorListBuilder();
        validateQuote(quoteTO, errorListBuilder);
        if (errorListBuilder.isFailure()) {
            return jsonWriter.writeJSON(errorListBuilder.buildResponse());
        }
        QuoteResponse response = quoteService.save(quoteTO, errorListBuilder);
        if (errorListBuilder.isFailure()) {
            return jsonWriter.writeJSON(errorListBuilder.buildResponse());
        }
        return jsonWriter.writeJSON(response);
    }

    private void validateQuote(QuoteTO quoteTO, ErrorListBuilder errorListBuilder) {
        if (StringUtils.isEmpty(quoteTO.getIsin()) || quoteTO.getIsin().length() != 12) {
            errorListBuilder.error(ErrorCode.ISIN_IS_EMPTY_OR_HAS_WRONG_LENGTH);
        }
        BigDecimal bid = quoteTO.getBid() == null ? BigDecimal.ZERO : quoteTO.getBid();
        BigDecimal ask = quoteTO.getAsk() == null ? BigDecimal.ZERO : quoteTO.getAsk();
        if (bid.compareTo(ask) >= 0) {
            errorListBuilder.error(ErrorCode.QUOTES_BID_AND_ASK_HAS_WRONG_VALUES);
        }
    }
}
