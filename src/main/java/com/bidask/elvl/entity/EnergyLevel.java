package com.bidask.elvl.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class EnergyLevel {
    private String isin;
    private BigDecimal value;
}