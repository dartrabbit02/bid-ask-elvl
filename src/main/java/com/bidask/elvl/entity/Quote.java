package com.bidask.elvl.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@Builder
public class Quote {
    private Date insertStamp;
    private String isin;
    private BigDecimal bid;
    private BigDecimal ask;
    private BigDecimal bidSize;
    private BigDecimal askSize;
}