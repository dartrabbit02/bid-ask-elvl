package com.bidask.elvl;

import com.bidask.elvl.dto.quote.QuoteTO;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class QuasiLoadTest extends ParentApplicationTest {

    private final Logger LOGGER = LoggerFactory.getLogger(QuasiLoadTest.class);
    private final int QUANTITY = 10000;
    private final int QUANTITY_FOR_WARMING = 9000;
    private final List<QuoteTO> quotes = new ArrayList<>();


    @Test
    public void quasiLoadTest() throws Exception {
        fill();
        warm();

        long start = System.nanoTime();
        for (int i = QUANTITY - QUANTITY_FOR_WARMING; i < QUANTITY; i++) {
            if (i % 7 == 0) {
                mvc.perform(MockMvcRequestBuilders
                        .post("/quotes/")
                        .content(jsonWriter.writeJSON(quotes.get(i)))
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .accept(MediaType.APPLICATION_JSON_VALUE));
            } else {
                mvc.perform(MockMvcRequestBuilders
                        .get("/energy-level/?isin={isin}", quotes.get(i).getIsin())
                        .accept(MediaType.APPLICATION_JSON_VALUE));
            }
        }
        long end = System.nanoTime();
        BigDecimal nanoSecondsPerRequest = BigDecimal.valueOf(end - start)
                .divide(BigDecimal.valueOf(QUANTITY-QUANTITY_FOR_WARMING), 2, RoundingMode.HALF_UP);
        BigDecimal requestsPerSecond = BigDecimal.ONE
                .divide(nanoSecondsPerRequest
                        .divide(BigDecimal.valueOf(1000000000),10, RoundingMode.HALF_UP),2, RoundingMode.HALF_UP);

        LOGGER.info("===============================================");
        LOGGER.info("[Nanoseconds per request] = {}", nanoSecondsPerRequest);
        LOGGER.info("[Requests per second] = {}", requestsPerSecond);
        LOGGER.info("===============================================");
    }

    private void fill() {
        for (int i = 0; i < QUANTITY; i++) {
            if (i % 10 == 0) {
                quotes.add(getWrongISINQuote());
            } else if (i % 9 == 0) {
                quotes.add(getIncorrectValuesQuote());
            } else {
                quotes.add(getCorrectValuesQuote());
            }
        }
    }

    private void warm() throws Exception {
        for (int i = 0; i < QUANTITY - QUANTITY_FOR_WARMING; i++) {
            mvc.perform(MockMvcRequestBuilders
                    .post("/quotes/")
                    .content(jsonWriter.writeJSON(quotes.get(i)))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE));
        }
    }
}
