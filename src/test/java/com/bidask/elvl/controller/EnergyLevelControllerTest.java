package com.bidask.elvl.controller;

import com.bidask.elvl.ParentApplicationTest;
import com.bidask.elvl.dto.BaseTO;
import com.bidask.elvl.dto.quote.QuoteTO;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

class EnergyLevelControllerTest extends ParentApplicationTest {


    @Test
    public void getEnergyLevelByISINTest() throws Exception {
        List<QuoteTO> quoteTOS = postFiveQuotes();
        Set<String> uniqueIsins = quoteTOS
                .stream()
                .map(BaseTO::getIsin)
                .collect(Collectors.toSet());

        for (String isin : uniqueIsins) {
            mvc.perform(MockMvcRequestBuilders
                    .get("/energy-level/?isin={isin}", isin)
                    .accept(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(content().string(containsString(isin)));
        }

        int energyLevelsInDB = energyLevelRepository.getAllEnergyLevels().size();
        assertEquals(energyLevelsInDB, uniqueIsins.size());
    }

    private List<QuoteTO> postFiveQuotes() throws Exception {
        List<QuoteTO> quotesList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            QuoteTO quoteTO = getCorrectValuesQuote();
            quotesList.add(quoteTO);
            mvc.perform(MockMvcRequestBuilders
                    .post("/quotes/")
                    .content(jsonWriter.writeJSON(quoteTO))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE));
        }
        return quotesList;
    }
}