package com.bidask.elvl.controller;

import com.bidask.elvl.ParentApplicationTest;
import com.bidask.elvl.dto.quote.QuoteTO;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

class QuoteControllerTest extends ParentApplicationTest {

    @Test
    public void postCorrectQuoteTest() throws Exception {
        QuoteTO quoteTO = getCorrectValuesQuote();
        mvc.perform(MockMvcRequestBuilders
                .post("/quotes/")
                .content(jsonWriter.writeJSON(quoteTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(containsString(quoteTO.getIsin())));
    }

    @Test
    public void postCorrectValuesQuoteTest() throws Exception {
        QuoteTO quoteTO = getIncorrectValuesQuote();
        mvc.perform(MockMvcRequestBuilders
                .post("/quotes/")
                .content(jsonWriter.writeJSON(quoteTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(containsString("Quote's 'bid' and 'ask' values are wrong")));
    }

    @Test
    public void postNullISINQuoteTest() throws Exception {
        QuoteTO quoteTO = getNullISINQuote();
        mvc.perform(MockMvcRequestBuilders
                .post("/quotes/")
                .content(jsonWriter.writeJSON(quoteTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(containsString("ISIN is empty or has wrong length")));
    }

    @Test
    public void postWrongISINQuoteTest() throws Exception {
        QuoteTO quoteTO = getWrongISINQuote();
        mvc.perform(MockMvcRequestBuilders
                .post("/quotes/")
                .content(jsonWriter.writeJSON(quoteTO))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().string(containsString("ISIN is empty or has wrong length")));
    }
}