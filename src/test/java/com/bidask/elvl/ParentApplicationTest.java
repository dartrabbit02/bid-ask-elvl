package com.bidask.elvl;

import com.bidask.elvl.dto.quote.QuoteTO;
import com.bidask.elvl.repository.EnergyLevelRepository;
import com.bidask.elvl.repository.QuoteRepository;
import com.bidask.elvl.typemanufacturer.QuoteBigDecimalTypeManufacturer;
import com.bidask.elvl.typemanufacturer.QuoteStringTypeManufacturer;
import com.bidask.elvl.utils.JSONWriter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.api.RandomDataProviderStrategy;
import uk.co.jemos.podam.api.RandomDataProviderStrategyImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public abstract class ParentApplicationTest {

    @Autowired
    protected EnergyLevelRepository energyLevelRepository;
    @Autowired
    protected QuoteRepository quoteRepository;
    @Autowired
    protected JSONWriter jsonWriter;
    @Autowired
    protected MockMvc mvc;
    private static List<String> isinList = new ArrayList<>();
    private static PodamFactory factory;

    @BeforeEach
    public void setUp() {
        isinList.addAll(Arrays.asList(
                "XS0461926569", "XS0462382069", "XS0466300257", "XS0484209159", "XS0493543986"));
        RandomDataProviderStrategy strategy = new RandomDataProviderStrategyImpl();
        strategy.addOrReplaceTypeManufacturer(BigDecimal.class, new QuoteBigDecimalTypeManufacturer());
        strategy.addOrReplaceTypeManufacturer(String.class, new QuoteStringTypeManufacturer(isinList));
        factory = new PodamFactoryImpl(strategy);
    }

    protected QuoteTO getCorrectValuesQuote() {
        QuoteTO quoteTO = factory.manufacturePojo(QuoteTO.class);
        quoteTO.setInsertStamp(null);
        if (quoteTO.getBid().compareTo(quoteTO.getAsk())>0) {
            swapBidAndAsk(quoteTO);
        }
        return quoteTO;
    }

    protected QuoteTO getIncorrectValuesQuote() {
        QuoteTO quoteTO = factory.manufacturePojo(QuoteTO.class);
        quoteTO.setInsertStamp(null);
        if (quoteTO.getBid().compareTo(quoteTO.getAsk())<0) {
            swapBidAndAsk(quoteTO);
        }
        return quoteTO;
    }

    protected QuoteTO getNullISINQuote() {
        QuoteTO quoteTO = factory.manufacturePojo(QuoteTO.class);
        quoteTO.setInsertStamp(null);
        quoteTO.setIsin(null);
        if (quoteTO.getBid().compareTo(quoteTO.getAsk())>0) {
            swapBidAndAsk(quoteTO);
        }
        return quoteTO;
    }

    protected QuoteTO getWrongISINQuote() {
        QuoteTO quoteTO = factory.manufacturePojo(QuoteTO.class);
        quoteTO.setInsertStamp(null);
        quoteTO.setIsin(quoteTO.getIsin().substring(3));
        if (quoteTO.getBid().compareTo(quoteTO.getAsk())>0) {
            swapBidAndAsk(quoteTO);
        }
        return quoteTO;
    }

    private void swapBidAndAsk(QuoteTO quoteTO) {
        BigDecimal swap = quoteTO.getAsk();
        quoteTO.setAsk(quoteTO.getBid());
        quoteTO.setBid(swap);
    }

}