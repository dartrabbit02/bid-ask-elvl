package com.bidask.elvl.typemanufacturer;

import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.typeManufacturers.TypeTypeManufacturerImpl;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class QuoteStringTypeManufacturer extends TypeTypeManufacturerImpl {

    private static final String ISIN = "isin";
    private List<String> isinList;

    public QuoteStringTypeManufacturer(List<String> isinList) {
        super();
        this.isinList = isinList;
    }

    @Override
    public Object getType(DataProviderStrategy strategy, AttributeMetadata attributeMetadata,
                          Map<String, Type> genericTypesArgumentsMap) {
        switch (attributeMetadata.getAttributeName()) {
            case ISIN:
                return isinList.get(new Random().nextInt(isinList.size()));
            default:
                return super.getType(strategy, attributeMetadata, genericTypesArgumentsMap);
        }
    }
}
