package com.bidask.elvl.typemanufacturer;

import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.typeManufacturers.TypeTypeManufacturerImpl;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

public class QuoteBigDecimalTypeManufacturer extends TypeTypeManufacturerImpl {
    private static final String BID = "bid";
    private static final String ASK = "ask";
    private static final String BIDSIZE = "bidSize";
    private static final String ASKSIZE = "askSize";

    @Override
    public Object getType(DataProviderStrategy strategy, AttributeMetadata attributeMetadata,
                          Map<String, Type> genericTypesArgumentsMap) {
        switch (attributeMetadata.getAttributeName()) {
            case BID:
                return generateRandomBigDecimal();
            case ASK:
                return generateRandomBigDecimal();
            case BIDSIZE:
                return generateRandomBigDecimal();
            case ASKSIZE:
                return generateRandomBigDecimal();
            default:
                return super.getType(strategy, attributeMetadata, genericTypesArgumentsMap);
        }
    }

    private BigDecimal generateRandomBigDecimal(){
        double leftLimit = 1F;
        double rightLimit = 1000F;
        double randomDouble = leftLimit + (float) Math.random()*(rightLimit-leftLimit);
        return BigDecimal.valueOf(randomDouble).setScale(2, RoundingMode.HALF_DOWN);
    }
}
